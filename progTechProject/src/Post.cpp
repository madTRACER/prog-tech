/*
 * Post.cpp
 *
 *  Created on: Jan 30, 2019
 *      Author: madtracer
 */

#include "Post.h"

Post::Post() {
	_postId = 0;
	_text = "";
	_imgPath = "";
	_creator = "";
}

Post::Post(int id, std::string creator, std::string imgPath, std::string text){
	setPostId(id);
	setCurDate();
	setText(text);
	setCreator(creator);
	setImgPath(imgPath);
}

Post::~Post() {
}

//------ Setters ------

bool Post::setPostId(int postId) {
	if (postId > 0){
		_postId = postId;
		return true;
	}
	else
		return false;

}

bool Post::setText(std::string text) {
	_text = text;
	return true;
}

bool Post::setImgPath(std::string imgPath) {
	if (imgPath[0] == '/'){
		_imgPath = imgPath;
		return true;
	}
	else
		return false;
}

bool Post::setCreator(std::string creator) {
	_creator = creator;
	return true;
}

bool Post::setLikes(int likes) {
	_likes = likes;
	return true;
}

//------ Getters -------

int Post::getPostId() {
	return _postId;
}

std::string Post::getText() {
	return _text;
}

std::string Post::getImgPath() {
	return _imgPath;
}

std::string Post::getCreator() {
	return _creator;
}

int Post::getLikes() {
	return _likes;
}
