/*
 * Post.h
 *
 *  Created on: Jan 30, 2019
 *      Author: madtracer
 */

#ifndef POST_H_
#define POST_H_

#include <string>
#include <iostream>
#include <cctype>
#include "Date.h"

class Post : public Date {
public:
	Post();
	Post(int id, std::string creator, std::string imgPath, std::string text);
	virtual ~Post();

	bool setPostId(int postId);
	bool setText(std::string text);
	bool setImgPath(std::string imgPath);
	bool setCreator(std::string creator);
	bool setLikes(int likes);

	int getPostId();
	std::string getText();
	std::string getImgPath();
	std::string getCreator();
	int getLikes();

private:
	std::string _imgPath;

protected:
	int _postId;
	std::string _text;
	std::string _creator;
	int _likes;
};

#endif /* POST_H_ */
