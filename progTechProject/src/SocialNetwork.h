 /*
 * SocialNetwork.h
 *
 *  Created on: Jan 29, 2019
 *      Author: madtracer
 */

#ifndef HEADERS_SOCIALNETWORK_H_
#define HEADERS_SOCIALNETWORK_H_

#include <string>
#include <iostream>
#include <cctype>
#include "User.h"
#include "Date.h"
#include "Database.h"

class SocialNetwork {
public:
	SocialNetwork();
	virtual ~SocialNetwork();

	bool setName(std::string name);
	bool setAddress(std::string address);

	std::string getName();
	std::string getAddress();

	bool initSocialNetwork(std::string url);
	bool authentication(string login, string password);
	bool exitSn();

	//------ Virtual methods------
	virtual void showMainMenu() = 0;
	virtual void showUserMenu() = 0;
	virtual void showPostMenu() = 0;
	virtual void showFriendMenu() = 0;
	virtual void showMessageMenu() = 0;

private:
	std::string _name;
	std::string _address;
protected:
	vector<User*> _users;
	vector<Post*> _posts;
	vector<Comment*> _comments;
	vector<int> _friends;
	vector<Message*> _messages;

	User *_lUser;
	User *_lFriend;
	Post *_lPost;
	Database *db;
};

#endif /* HEADERS_SOCIALNETWORK_H_ */
