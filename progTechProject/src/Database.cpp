/*
 * Database.cpp
 *
 *  Created on: Feb 12, 2019
 *      Author: madtracer
 */

#include "Database.h"

Database::Database() {
	_url = "";
}

Database::Database(string url){
	_url = url;
}

Database::~Database() {

}

//--------------------------------------------READING METHODS------------------------------------------------

//------Users------

vector<User*> Database::getAllUsers() {
	vector<User*> allUsers;
	_fin.open(_url + "_users.txt");
	if (_fin.is_open()){
		while (!_fin.eof()){
			User *curUser = new User;
			vector<int> friends;
			int bufFr;
			int id, num;
			std::string login, username, password, role;
			_fin >> id >> username >> login >> password >> role >> num;
			for (int i = 0; i < num; i++) {
				_fin >> bufFr;
				friends.push_back(bufFr);
			}


			curUser->setUserId(id);
			curUser->setLogin(login);
			curUser->setUsername(username);
			curUser->setPassword(password);
			curUser->setRole(role);
			curUser->setFriends(friends);

			allUsers.push_back(curUser);
		}
	}
	_fin.close();
	return allUsers;
}


//User* Database::getUserByLogin(string sLogin) {
//	User *sUser = new User;
//	_fin.open("users.txt");
//	if (_fin.is_open()){
//		while (!_fin.eof()){
//			int id;
//			std::string login, username, password, role;
//			_fin >> id >> username >> login >> password >> role;
//			if (login == sLogin) {
//				sUser->setUserId(id);
//				sUser->setLogin(login);
//				sUser->setUsername(username);
//				sUser->setPassword(password);
//				sUser->setRole(role);
//			}
//		}
//	}
//	_fin.close();
//
//	return sUser;
//}
//
//User* Database::userAuthentication(string sLogin, string sPassword) {
//	User *sUser = new User;
//		_fin.open("users.txt");
//		if (_fin.is_open()){
//			while (!_fin.eof()){
//				int id;
//				std::string login, username, password, role;
//				_fin >> id >> username >> login >> password >> role;
//				if (login == sLogin && password == sPassword) {
//					sUser->setUserId(id);
//					sUser->setLogin(login);
//					sUser->setUsername(username);
//					sUser->setPassword(password);
//					sUser->setRole(role);
//				}
//			}
//		}
//		_fin.close();
//
//		return sUser;
//}

//------Posts------

//Post* Database::getPostById(int sId) {
//	Post *sPost = new Post;
//		_fin.open(_url + "_posts.txt");
//		if (_fin.is_open()){
//			while (!_fin.eof()){
//				int id;
//				std::string creator, imgPath, text, date;
//				_fin >> id >> creator >> date >> imgPath >> text;
//				if (id == sId) {
//					sPost->setPostId(id);
//					sPost->setCreator(creator);
//					sPost->setImgPath(imgPath);
//					sPost->setText(text);
//					sPost->setDate(date);
//				}
//			}
//		}
//		_fin.close();
//
//		return sPost;
//}

vector<Post*> Database::getPostsByCreator(string sCreator) {
	vector<Post*> sPosts;
	_fin.open(_url + "_posts.txt");
	if (_fin.is_open()){
		while (!_fin.eof()){
			Post *curPost = new Post;
			int id, likes;
			std::string creator, imgPath, text, date;
			_fin >> id >> creator >> date >> imgPath >> likes;
			getline(_fin, text);
			if (creator == sCreator) {
				curPost->setPostId(id);
				curPost->setCreator(creator);
				curPost->setImgPath(imgPath);
				curPost->setText(text);
				curPost->setDate(date);
				curPost->setLikes(likes);
			}

			sPosts.push_back(curPost);
		}
	}
	_fin.close();
	return sPosts;
}

vector<Post*> Database::getPostsByDate(string sDate) {
	vector<Post*> sPosts;
	_fin.open(_url + "_posts.txt");
	if (_fin.is_open()){
		while (!_fin.eof()){
			Post *curPost = new Post;
			int id;
			std::string creator, imgPath, text, date;
			_fin >> id >> creator >> date >> imgPath;
			getline(_fin, text);
			if (date == sDate) {
				curPost->setPostId(id);
				curPost->setCreator(creator);
				curPost->setImgPath(imgPath);
				curPost->setText(text);
				curPost->setDate(date);
			}

			sPosts.push_back(curPost);
		}
	}
	_fin.close();
	return sPosts;
}

//------Comments------

vector<Comment*> Database::getCommentsByPostId(int sPostId){
	vector<Comment*> sComments;
	_fin.open(_url + "_comments.txt");
	if (_fin.is_open()){
		while (!_fin.eof()){
			Comment *curComment = new Comment;
			int id, parentPostId;
			std::string creator, text, date;
			_fin >> id >> parentPostId >> creator >> date;
			getline(_fin, text);
			if (parentPostId == sPostId) {
				curComment->setPostId(id);
				curComment->setCreator(creator);
				curComment->setParentPostId(parentPostId);
				curComment->setText(text);
				//curComment->setDate(date);
			}

			sComments.push_back(curComment);
		}
	}
	_fin.close();
	return sComments;
}

//------Messages------

std::vector<Message*> Database::getDialog(std::string sSender, std::string sRecipient) {
vector<Message*> sMessages;
	_fin.open(_url + "_messages.txt");
	if (_fin.is_open()){
		while (!_fin.eof()){
			Message *curMessage;
			int id;
			std::string sender, recipient, text;
			_fin >> id >> sender >> recipient;
			getline(_fin, text);
			if (sender == sSender && recipient == sRecipient) {
				curMessage = new Message(id, sender, recipient, text);
			}

			sMessages.push_back(curMessage);
		}
	}
	_fin.close();
	return sMessages;
}



//--------------------------------------------WRITING METHODS------------------------------------------------

//------Users------
bool Database::pushUsers(vector<User*> pUsers) {
	_fout.open(_url + "_users.txt", ios::app);
	if (_fout.is_open()){
		for (int i = 0; i < pUsers.size(); i++){
			vector<int> friends = pUsers[i]->getFriends();
			_fout << pUsers[i]->getUserId() << " " << pUsers[i]->getUsername() << " " << pUsers[i]->getLogin() << " " << pUsers[i]->getPassword() << " " << pUsers[i]->getRole() << " ";
			for (int i = 0; i < friends.size(); i++)
				cout << friends[i];
			cout << endl;
		}
		_fout.close();
		return true;
	}
	else {
		return false;
	}
}
//------Posts------
bool Database::pushPosts(vector<Post*> pPosts) {
	_fout.open(_url + "_posts.txt");
	if (_fout.is_open()){
		for (int i = 0; i < pPosts.size(); i++)
		_fout << pPosts[i]->getPostId() << " " << pPosts[i]->getCreator() << " " << pPosts[i]->getDate() << " " << pPosts[i]->getImgPath() << " " << pPosts[i]->getText() << " " << pPosts[i]->getLikes() << endl;
		_fout.close();
		return true;
	}
	else {
		return false;
	}
}
//------Comments------
bool Database::pushComments(vector<Comment*> pComments) {
	_fout.open(_url + "_comments.txt");
	if (_fout.is_open()){
		for (int i = 0; i < pComments.size(); i++)
			_fout << pComments[i]->getPostId() << " " << pComments[i]->getParentPostId() << " " << pComments[i]->getCreator() << " " << pComments[i]->getDate() << " " << pComments[i]->getText() << endl;
		_fout.close();
		return true;
	}
	else {
		return false;
	}
}

bool Database::pushMessages(vector<Message*> pMessages) {
	_fout.open(_url + "_messages.txt");
	if (_fout.is_open()){
		for (int i = 0; i < pMessages.size(); i++)
			_fout << pMessages[i]->getMessageId() << " " << pMessages[i]->getSender() << " " << pMessages[i]->getRecipient() << " " << pMessages[i]->getText() << endl;
	}
}
