/*
 * User.cpp
 *
 *  Created on: Jan 29, 2019
 *      Author: madtracer
 */

#include "User.h"


User::User() {
	_userId = 0;
	_login = "";
	_password = "";
	_username = "";
	_role = "";
}

User::User(int id, std::string login, std::string password, std::string role, std::string username, std::vector<int> friends){
	setLogin(login);
	setPassword(password);
	setRole(role);
	setUsername(username);
	setUserId(id);
	setCurDate();
	setFriends(friends);
}

User::User(int id, std::string login, std::string password, std::string role, std::string username){
	setLogin(login);
	setPassword(password);
	setRole(role);
	setUsername(username);
	setUserId(id);
	setCurDate();
}


User::~User() {
}

//------ Setters------

bool User::setFriends(std::vector<int> friends) {
	_friends = friends;
	return true;
}

bool User::setUserId(int userId) {
	if (userId > 0){
		_userId = userId;
		return true;
	}
	else
		return false;
}

bool User::setLogin(std::string login) {
	if (login.length() > 2){
		_login = login;
		return true;
	}
	else
		return false;
}

bool User::setPassword(std::string password) {
	bool digit = false, sLetter = false, cLetter = false;

	for (int i = 0; i < password.length(); i++){
		if (isdigit(password[i]))
			digit = true;
		else if (isupper(password[i]))
			cLetter = true;
		else if (islower(password[i]))
			sLetter = true;
	}

	if (digit && sLetter && cLetter){
		_password = password;
		return true;
}
	else
		return false;
}

bool User::setUsername(std::string username) {
	for (int i = 0; i < username.length(); i++) {
		if (!isalpha(username[i])){
			return false;
		}
	}
	_username = username;
	return true;
}

bool User::setRole(std::string role) {
	_role = role;
	return true;
}

//------ Getters ------

std::vector<int> User::getFriends() {
	return _friends;
}

int User::getUserId() {
	return _userId;
}

std::string User::getLogin() {
	return _login;
}

std::string User::getPassword() {
	return _password;
}

std::string User::getUsername() {
	return _username;
}

std::string User::getRole() {
	return _role;
}


