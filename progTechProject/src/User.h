/*
 * User.h
 *
 *  Created on: Jan 29, 2019
 *      Author: madtracer
 */

#ifndef HEADERS_USER_H_
#define HEADERS_USER_H_

#include <string>
#include <iostream>
#include <cctype>
#include <vector>
#include "Post.h"
#include "Date.h"
//#include "Database.h"

class User : public Date {
public:
	User();
	User(int id, std::string login, std::string password, std::string role, std::string username);
	User(int id, std::string login, std::string password, std::string role, std::string username, std::vector<int> friends);
	virtual ~User();

	bool setUserId(int userId);
	bool setLogin(std::string login);
	bool setPassword(std::string password);
	bool setUsername(std::string username);
	bool setRole(std::string role);
	bool setFriends(std::vector<int> friends);

	int getUserId();
	std::string getLogin();
	std::string getPassword();
	std::string getUsername();
	std::string getRole();
	std::vector<int> getFriends();

private:
	int _userId;
	std::string _login;
	std::string _password;
	std::string _username;
	std::string _role;
	//vector<Post*> _posts;
	std::vector<int> _friends;

protected:
};

#endif /* HEADERS_USER_H_ */
